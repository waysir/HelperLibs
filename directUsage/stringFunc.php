<?php
//********************client_common_control
function www_setcookie($var, $value, $life = 0, $prefix = 1) {
	global $cookiepre, $cookiedomain, $cookiepath, $timestamp, $_SERVER;
	header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
	setcookie(($prefix ? $cookiepre : '').$var, $value,
		$life ? $timestamp + $life : 0, $cookiepath,
		$cookiedomain, $_SERVER['SERVER_PORT'] == 443 ? 1 : 0);
}
//前端标签操作
function toHtml($txt){
	$txt = str_replace("  ","　",$txt);
	$txt = str_replace("<","&lt;",$txt);
	$txt = str_replace(">","&gt;",$txt);
	$txt = preg_replace("/[".EOL."]{1,}/is","<br/>",$txt);
	return $txt;
}

function delHtml($txt){
	$txt = str_replace("<p>","",$txt);
	$txt = str_replace("</p>","",$txt);
	return $txt;
}

//把字符串转化为可以在JS数组里输出的字符串
function jsToHtml($mystr){
	if(empty($mystr)){
		return "";
	}
	$mystr = str_replace(EOL,"",$mystr);
	$mystr = str_replace("'","\\'",$mystr);
	return $mystr;
}

/**
 *	功能：判断是否中文字符
 *
 *	$mystr 	要判断的字符串
 *	返回：true/false
 */
function check_is_cn($mystr){
	if(preg_match("/[".chr(228).chr(128).chr(128)."-".chr(233).chr(191).chr(191)."]/",$mystr)){
		return true;
	}else{
		return false;
	}
}

/*
	功能:按长度截取字符串[兼容中文]
	$string, $length, $dot,$charset
	字符串,要取长度,截断符,编码
*/
function cutstr($string, $length, $dot = ' ...',$charset='utf-8') {

	if(strlen($string) <= $length) {
		return $string;
	}

	$string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array('&', '"', '<', '>'), $string);

	$strcut = '';
	if(strtolower($charset) == 'utf-8') {

		$n = $tn = $noc = 0;
		while($n < strlen($string)) {

			$t = ord($string[$n]);
			if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
				$tn = 1; $n++; $noc++;
			} elseif(194 <= $t && $t <= 223) {
				$tn = 2; $n += 2; $noc += 2;
			} elseif(224 <= $t && $t <= 239) {
				$tn = 3; $n += 3; $noc += 2;
			} elseif(240 <= $t && $t <= 247) {
				$tn = 4; $n += 4; $noc += 2;
			} elseif(248 <= $t && $t <= 251) {
				$tn = 5; $n += 5; $noc += 2;
			} elseif($t == 252 || $t == 253) {
				$tn = 6; $n += 6; $noc += 2;
			} else {
				$n++;
			}

			if($noc >= $length) {
				break;
			}
		}
		if($noc > $length) {
			$n -= $tn;
		}
		$strcut = substr($string, 0, $n);

	} else {
		for($i = 0; $i < $length; $i++) {
			$strcut .= ord($string[$i]) > 127 ? $string[$i].$string[++$i] : $string[$i];
		}
	}
	$strcut = str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), $strcut);

	return $strcut.$dot;
}

/*
功能:邮箱地址
$email,
字符串
*/
function isemail($email) {

	return strlen($email) > 6 && preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $email);
}

//=========================begin 时间处理
/**
 * 功能:检查是否为一个合法的时间格式
 * @param   string  $time
 * @return  void
 */
function is_time($time){

	$pattern = '/[\d]{4}-[\d]{1,2}-[\d]{1,2}\s[\d]{1,2}:[\d]{1,2}:[\d]{1,2}/';
	
	return preg_match($pattern, $time);
}
/**
 * 功能:检查是否为一个合法的日期格式
 * @param   string  $date
 */
function is_date($date){

	$pattern = '/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/';
	return preg_match($pattern, $date);
}

/**
*功能:返回两个时间差
*参数:$time,$time2 时间1,时间2
*参数:$myformat 返回的时间差格式
*/
function change_format($time,$time2,$myformat="d"){
	$sY = 31536000;//年
	$sM = 2592000;//月(三十天计算)
	$sW = 604800;//星期
	$sD = 86400;//天
	$sH = 3600;//小时
	$sI = 60;//分钟
	$sS = 1;//秒

	$tmp_time = strtotime($time)-strtotime($time2);
	switch($myformat){
		case "y"://年
			$tmp_time = $tmp_time/$sY;
			break;
		case "w"://星期
			$tmp_time = $tmp_time/$sW;
			break;
		case "d"://天
			$tmp_time = $tmp_time/$sD;
			break;
		case "h"://小时
			$tmp_time = $tmp_time/$sH;
			break;
		case "i"://分钟
			$tmp_time = $tmp_time/$sI;
			break;
		case "s"://秒
			$tmp_time = $tmp_time;
			break;
		default://默认返回天
			$tmp_time = $tmp_time/$sD;
			break;
	}
	return $tmp_time;
}