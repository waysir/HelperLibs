<?php
///////////////////////// 数据库类 begin
	/* *
	*	检查相关记录是否存在
	*
	*	$table 数据库表名
	*	$value 与$fields对应的值
	* */
	function exist_check($conn,$table,$value = array()){
		$where = '';
		foreach($value as $key=>$val){
			if($key!=""){
				$where .= " AND `".$key."`='".$val."' ";
			}
		}

		$exist= "SELECT 1 FROM ".get_table($table)." WHERE 1 ".$where;
		//die($exist);
		$res = $conn->Query($exist);
		if($conn->NumRows($res)>0)
		{
			return true;
		}else{
			return false;
		}
	}


	/* *
	*	在数据库表$table中增加一条记录
	*
	*	$table 数据库表名
	*	$value 与$field对应的值
	* */
	function add_record($conn,$table,$value = array(),$show_bug=false)
	{	
		$field_str = '';
		$value_str = '';
		foreach($value as $key=>$val)
		{
			if($key!=""){
				$field_str .= '`'.$key.'`,';	
				$value_str .= "'".$val."',";
			}
		}
		
		$field_str = substr($field_str,0,-1);
		$value_str = substr($value_str,0,-1);
		
		$insert_sql = "INSERT INTO ".get_table($table)." ($field_str) VALUES($value_str);";
		//echo $insert_sql;
		$conn->Query($insert_sql);
		
		$result = array();
		$result['rows'] = $conn->AffectedRows();
		$result['id'] = $conn->InsertID();
		if($show_bug)
			$result['error'] = $insert_sql;

		return $result;
	}

	/* *
	*	在数据库表$table中更新一条记录
	*
	*	$table 	数据库表名
	*	$value 	set对应的键值对
	*	$where_value	条件
	*	$where 			条件（优先）
	* */

	function update_record($conn,$table,$value = array(),$where_value = array(),$where='')
	{	
		$update_str = '';
		
		foreach($value as $key=>$val)
		{
			if($key!=""){
				if(strstr($val,$key.'+')!=false){
					$update_str .= "`".$key."`=".$val.",";
				}else{
					$update_str .= "`".$key."`='".$val."',";
				}
				
			}
		}
		if(empty($where))
		{
			foreach($where_value as $key=>$val)
			{
				if($key!=""){
					$where .= " AND `".$key."`='".$val."' ";
				}
			}
		}
		
		$update_str = substr($update_str,0,-1);
		
		$sql = "UPDATE ".get_table($table)."
			SET $update_str
			WHERE 1 $where;";
		// echo $sql;exit;
		$rs = $conn->Query($sql);
		return $conn->AffectedRows();
	}

	/* *
	*	在数据库表$table中删除(物理删除)或更新(逻辑删除)一条记录
	*
	*	$table 	数据库表名
	*	$where_ 条件字段与值
	*	$value 	与$field对应的值
	* */

	function delete_record($conn,$table,$where_value = array(),$value = array())
	{	
		$update_str = '';
		$where = '';
		foreach($where_value as $key=>$val)
		{
			if($key!=""){
				$where .= " AND `".$key."`='".$val."' ";
			}
		}
		if(count($value)>0)
		{
			foreach($value as $key=>$val)
			{
				if($key!=""){
					$update_str .= "`".$key."`='".$val."',";
				}		
			}
			
			$update_str = substr($update_str,0,-1);
			
			$sql = "UPDATE ".get_table($table)." SET $update_str WHERE 1 $where;";
		}
		else
		{
			$sql = "DELETE FROM ".get_table($table)." WHERE 1 $where;";
		}

		$conn->Query($sql);
		
		return $conn->AffectedRows();
	}

	/**
	 *	获取数据库表$table中字段$info的信息
	 *	
	 * 	$conn	数据库链接
	 *	$table	数据库表名
	 *	$selectCol	查询信息字段名
	 *	$where	条件字段名
	 *	$OrderBy	字段($fields)对应值
	 *	$all	true多条,(默认)false-一条
	 *	
	 *	@update	 ljw 20170722
	 */
	function get_info($conn,$table,$selectCol = [],$where='',$OrderBy='',$all = false)
	{
		if(!empty($selectCol))
		{
			$str = implode(',',$selectCol);
		}
		else
		{
			$str = '*';
		}
		if ($OrderBy != ""){
			$where .= $OrderBy;
		}
		$sql= "SELECT $str FROM ".get_table($table)." WHERE 1 ".$where.'';
		$res = $conn->Query($sql);
		// if($table == 'area')	die($sql);
		if($conn->NumRows($res)>0)
		{
			if($all)
			{
				$all_record = [];//$all_record[] = $arr;
				while($arr = $conn->FetchArray($res))
				{
					$all_record[] = $arr;
				}			
				return $all_record;
			}
			else
			{			
				$arr = $conn->FetchArray($res);
				return $arr;
			}
		}
		else
		{
			return [];
		}
	}

	function get_table($myname){
		global $TABLE_NAME_INC,$MYSQL_DB;
		return $MYSQL_DB.".`".$TABLE_NAME_INC.$myname."`";
	}
//=========end 操作数据库通用