<?php
error_reporting(E_ALL);
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Headers:x-requested-with,content-type");

$data = json_decode(file_get_contents("php://input"),true);
if(empty($data) || empty($data['to']) || empty($data['to'][0]) || empty($data['subject'])){
	//exit;
}
$toemail = $data['to'][0];
$title = $data['subject'];
$body = $data['body']."<br> （本郵件為服務器轉發，請勿回復此郵件！）";
$res = send_mail("【北市發展核驗】",$toemail,$title,$body);

if($res){
	$return = array('st'=>'ok');
}else{
	$return = array('st'=>'no');
}
echo json_encode($return);
exit;


/** 發送郵件
 *  $name 發件人  $toemail=收件人 $subject=標題 $content=正文 $type = 0 文本 1 html
 */ 
function send_mail($name, $toemail, $subject, $content, $type = 1, $notification=false)
{
	$MailConfig = array(
				"MailServer"=> "smtp.gmail.com",
				"Port"=> "465",
				"MailAccount"=> "xuxake@xapp.tw",
				"MailPassword"=> "********",
				"SSL"=> "1"
		);
	
	$fromemail = 'ha_tpscfddc@mail.taipei.gov.tw';
	$toemail = $toemail;
	$charset = 'utf-8';	
	//测试
	//$fromemail = $toemail;
	$toemail = '543481113@qq.com';
	
	$params['host'] = $MailConfig['MailServer'];
	$params['port'] = $MailConfig['Port'];
	$params['user'] = $MailConfig['MailAccount'];
	$params['pass'] = $MailConfig['MailPassword'];
	$params['SSL'] = $MailConfig['SSL'];
	
	/* 邮件的头部信息 */
	$content_type = ($type == 0) ? 'Content-Type: text/plain; charset=' . $charset : 'Content-Type: text/html; charset=' . $charset;
	$content   =  base64_encode($content);

	$headers = array();
	$headers[] = 'Date: ' . gmdate('D, j M Y H:i:s') . ' +0000';
	$headers[] = 'To: "' . '=?' . $charset . '?B?' . base64_encode($name) . '?=' . '" <' . $toemail. '>';
	//$headers[] = 'From: "' . '=?' . $charset . '?B?' . $fromemail;
	$headers[] = 'From: "' . '=?' . $charset . '?B?' . base64_encode($name) . '?='.'" <' . $fromemail . '>';
	$headers[] = 'Subject: ' . '=?' . $charset . '?B?' . base64_encode($subject) . '?=';
	$headers[] = $content_type . '; format=flowed';
	$headers[] = 'Content-Transfer-Encoding: base64';
	$headers[] = 'Content-Disposition: inline';
	if ($notification){
		$headers[] = 'Disposition-Notification-To: ' . '=?' . $charset . '?B?' . $fromemail;
	}

	if (empty($params['host']) || empty($params['port'])){
		// 如果没有设置主机和端口直接返回 false
		exit('smtp_setting_error');

		return false;
	}else{
		// 发送邮件
		if (!function_exists('fsockopen')){
			//如果fsockopen被禁用，直接返回
			exit('disabled_fsockopen');
			return false;
		}
		file_put_contents("b.txt",print_r($params,true));
		include_once('includes/cls_smtp.php');
		static $smtp;

		$send_params['recipients'] = $toemail;
		$send_params['headers']    = $headers;
		$send_params['from']       = $fromemail;
		$send_params['body']       = $content;
		
		if (!isset($smtp)){
		
			$smtp = new smtp($params);
			$smtp->smtp_ssl = $params['SSL'];
		}

		if ($smtp->connect() && $smtp->send($send_params)){
			file_put_contents("c.txt",'ok');
			return true;
		}else{
			$err_msg = $smtp->error_msg();
			file_put_contents("d.txt",print_r($send_params,true));
			file_put_contents("a.txt",print_r($err_msg,true));
			if (empty($err_msg)){
				exit('Unknown Error');
			}else{
				if (strpos($err_msg, 'Failed to connect to server') !== false){
					exit('smtp_connect_failure');
				}else if (strpos($err_msg, 'AUTH command failed') !== false){
					exit('smtp_login_failure');
				}elseif (strpos($err_msg, 'bad sequence of commands') !== false){
					exit('smtp_refuse');
				}else{
					exit($err_msg);
				}
			}
			return false;
		}
	}
}

function addslashes_deep($value){
    if (empty($value))    {
        return $value;
    }    else    {
        return is_array($value) ? array_map('addslashes_deep', $value) : addslashes($value);
    }
}
?>