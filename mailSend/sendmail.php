<?php
	error_reporting(E_ALL);
	header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:x-requested-with,content-type");

	$data = json_decode(file_get_contents("php://input"),true);
	if(empty($data) || empty($data['to']) || empty($data['to'][0]) || empty($data['subject'])){
		//exit;
	}
	$toemail = $data['to'][0];
	$title = $data['subject'];
	$body = $data['body']."<br> （本郵件為服務器轉發，請勿回復此郵件！）";
	// $res = send_mail("【北市發展核驗】",$toemail,$title,$body);
	$res = send_mail("【莫言一封】",'916617109@qq.com','testMailer','Now!!!!!');

	if($res){
		$return = array('st'=>'ok');
	}else{
		$return = array('st'=>'no');
	}
	echo json_encode($return);
	exit;


	/** 發送郵件
	 *  $name 發件人  $toemail=收件人 $subject=標題 $content=正文 $type = 0 文本 1 html
	 */
	function send_mail($name, $toemail, $subject, $content, $type = 1, $notification=false)
	{
		include 'PHPMailer/src/Exception.php';
		include 'PHPMailer/src/SMTP.php';
		include 'PHPMailer/src/PHPMailer.php';
		$mail = new PHPMailer(true);// Passing `true` enables exceptions

		//Server settings
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Host = 'smtp.126.com';  // Specify main and backup SMTP servers
		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true,
		    )
		);//gmail_server 的验证需要此参数,其他不用
		$mail->Port = 465;                                    // TCP port to connect to
		$mail->Username = 'ljwnet122@126.com';                 // SMTP username
		$mail->Password = 'qwe789DEV';                           // SMTP password

		$mail->setFrom('ljwnet122@126.com', $name);
		//Recipients
		$mail->addAddress($toemail, '敬爱的客户');     //添加收件人
		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->CharSet  ="utf-8";
		$mail->Subject = $subject;
		$mail->Body    = $content;
		if($type == 0){
			$mail->AltBody = addslashes_deep($content);
		}

		try {
		    $mail->send();
		    $res = true;
		} catch (Exception $e) {
			if($mail->SMTPDebug != 0){
				echo '<pre>';
				var_export($e);
				echo '</pre>';
			}
		    $res = false;
		}
		return $res;
	}

	function addslashes_deep($value){
	    if (empty($value))    {
	        return $value;
	    }    else    { //递归处理
	        return is_array($value) ? array_map('addslashes_deep', $value) : addslashes($value);
	    }
	}
